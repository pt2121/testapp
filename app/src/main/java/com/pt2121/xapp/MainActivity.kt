package com.pt2121.xapp

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.SpannableString
import android.text.method.LinkMovementMethod
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.work.Constraints.Builder
import androidx.work.WorkInfo.State.SUCCEEDED
import androidx.work.WorkManager
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import com.pt2121.xapp.RetrievingLocationWork.Companion.KEY_DATA_LOCATION
import com.pt2121.xapp.utils.addClickableSpan
import com.pt2121.xapp.workmanager.PeriodicWorkScheduler
import dagger.android.AndroidInjection
import javax.inject.Inject

class MainActivity : AppCompatActivity(),
    LocationPermissionRequestDelegate.Listener {

    private lateinit var textView: TextView

    @Inject
    lateinit var permissionRequestDelegate: LocationPermissionRequestDelegate

    @Inject
    lateinit var periodWorkScheduler: PeriodicWorkScheduler

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        textView = findViewById(R.id.id_text_view)

        val hello = getString(R.string.hello)

        val spannableString = SpannableString(textView.text)
        spannableString.addClickableSpan(hello) {
            Toast.makeText(this, R.string.task_1_completed, Toast.LENGTH_LONG).show()
        }

        with(textView) {
            text = spannableString
            movementMethod = LinkMovementMethod.getInstance()
        }

        val googleApiAvailability = GoogleApiAvailability.getInstance()
        val code = googleApiAvailability.isGooglePlayServicesAvailable(this)
        if (code != ConnectionResult.SUCCESS) {
            googleApiAvailability.getErrorDialog(this, code, GOOGLE_PLAY_REQUEST_CODE).show()
        }

        permissionRequestDelegate.setListener(this)
    }

    override fun onPermissionGranted() {
        startLocationWork()
    }

    private fun startLocationWork() {
        val constraints = Builder().setRequiresBatteryNotLow(true).build()
        periodWorkScheduler.startWork(
            RetrievingLocationWork::class.java,
            constraints,
            BuildConfig.LOCATION_RETRIEVAL_INTERVAL
        )

        WorkManager.getInstance()
            .getWorkInfosForUniqueWorkLiveData(RetrievingLocationWork::class.java.name)
            .observe(this, Observer { infoList ->
                infoList.lastOrNull()?.let { info ->
                    if (info.state == SUCCEEDED) {
                        info.outputData.let { data ->
                            data.getString(KEY_DATA_LOCATION)?.let { locationString ->
                                Toast.makeText(this, locationString, Toast.LENGTH_LONG).show()
                                // TODO: Task #4 - Local storage options
                                // We could persist the location locally in the SQLite database (Room/ContentProvider), SharedPreferences or a 3rd-party database e.g. Realm.
                                // It depends on a use case.
                            }
                        }
                    }
                }
            })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        // I don't have a device to test for this flow.
        if (requestCode == GOOGLE_PLAY_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            permissionRequestDelegate.requestPermissionIfNeeded()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        permissionRequestDelegate.onRequestPermissionsResult(requestCode, grantResults)
    }

    companion object {
        private const val GOOGLE_PLAY_REQUEST_CODE = 3290
    }
}

