package com.pt2121.xapp.di

import androidx.lifecycle.ProcessLifecycleOwner
import com.pt2121.xapp.BuildConfig
import com.pt2121.xapp.workmanager.PeriodWorkSchedulerImpl
import com.pt2121.xapp.workmanager.PeriodWorkSchedulerImplDebug
import com.pt2121.xapp.workmanager.PeriodicWorkScheduler
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
object AppModule {

    /**
     * provide different implementations of [PeriodicWorkScheduler] depending on a build type.
     */
    @Provides
    @Singleton
    @JvmStatic
    fun providePeriodicWorkScheduler(): PeriodicWorkScheduler =
        if (BuildConfig.DEBUG) {
            PeriodWorkSchedulerImplDebug(ProcessLifecycleOwner.get())
        } else {
            PeriodWorkSchedulerImpl()
        }
}