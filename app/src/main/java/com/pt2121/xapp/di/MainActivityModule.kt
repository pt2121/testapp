package com.pt2121.xapp.di

import androidx.appcompat.app.AppCompatActivity
import com.pt2121.xapp.MainActivity
import dagger.Binds
import dagger.Module

@Module
abstract class MainActivityModule {

    @Binds
    abstract fun provideActivity(mainActivity: MainActivity): AppCompatActivity
}