package com.pt2121.xapp

import android.content.Context
import androidx.work.Data
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.pt2121.xapp.utils.LocationUtils.getBestLastLocation

/**
 * Worker job to retrieve the last known location
 *
 * Task #3
 * WorkManager meets the requirement.
 * The task is deferrable and doesn't need to run at a precise time
 * and expected to run even if your device or application restarts.
 */
class RetrievingLocationWork(
    context: Context, params: WorkerParameters
) : Worker(context, params) {

    override fun doWork(): Result {
        val lastLocation = getBestLastLocation(applicationContext)

        return lastLocation?.let { location ->
            Result.success(Data.Builder().putString(KEY_DATA_LOCATION, location.toString()).build())
        } ?: Result.failure()
    }

    companion object {
        const val KEY_DATA_LOCATION = "key_data_location"
    }
}