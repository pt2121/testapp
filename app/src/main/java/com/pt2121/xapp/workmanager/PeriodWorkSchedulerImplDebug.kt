package com.pt2121.xapp.workmanager

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.work.Constraints
import androidx.work.ExistingWorkPolicy
import androidx.work.ListenableWorker
import androidx.work.OneTimeWorkRequest
import androidx.work.WorkManager
import java.util.concurrent.TimeUnit

/**
 * An implementation of the `PeriodWorkScheduler` for debugging a recurring task
 * that scheduled more frequently than `PeriodicWorkRequest.MIN_PERIODIC_INTERVAL_MILLIS`.
 *
 * Unfortunately, the minimum interval duration of `PeriodicWorkRequest` is 15 minutes
 * so the implementation of this class is different from the non-debug version!
 */
class PeriodWorkSchedulerImplDebug(
    private val lifecycleOwner: LifecycleOwner
) : PeriodicWorkScheduler {

    /**
     * Start a recurring task, `workerClass` with the `constraints`.
     */
    override fun startWork(
        workerClass: Class<out ListenableWorker>,
        constraints: Constraints,
        intervalInMinutes: Long
    ) {
        val requestBuilder = OneTimeWorkRequest.Builder(workerClass)
            .setConstraints(constraints)

        val workManager = WorkManager.getInstance()

        workManager.enqueueUniqueWork(
            workerClass.name,
            ExistingWorkPolicy.REPLACE,
            requestBuilder.build()
        )

        workManager.getWorkInfosForUniqueWorkLiveData(workerClass.name)
            .observe(lifecycleOwner, Observer { infoList ->
                infoList?.let {
                    workManager.enqueueUniqueWork(
                        workerClass.name,
                        ExistingWorkPolicy.KEEP,
                        requestBuilder.setInitialDelay(15, TimeUnit.SECONDS).build()
                    )
                }
            })
    }
}