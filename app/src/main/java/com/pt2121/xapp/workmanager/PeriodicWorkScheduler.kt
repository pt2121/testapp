package com.pt2121.xapp.workmanager

import androidx.work.Constraints
import androidx.work.ListenableWorker

/**
 * An interface for scheduling a periodic task.
 *
 * Ideally, this probably should be decoupled from `WorkManager`.
 */
interface PeriodicWorkScheduler {

    /**
     * Schedule a work of `workerClass`.
     *
     * An existing pending (uncompleted) work with the same unique name would be replaced.
     */
    fun startWork(
        workerClass: Class<out ListenableWorker>,
        constraints: Constraints,
        intervalInMinutes: Long
    )
}