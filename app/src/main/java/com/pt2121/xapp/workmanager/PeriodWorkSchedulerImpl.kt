package com.pt2121.xapp.workmanager

import androidx.work.Constraints
import androidx.work.ExistingPeriodicWorkPolicy
import androidx.work.ListenableWorker
import androidx.work.PeriodicWorkRequest
import androidx.work.WorkManager
import java.util.concurrent.TimeUnit.MINUTES

/**
 * A wrapper of the `WorkManager` for scheduling a periodic task.
 */
class PeriodWorkSchedulerImpl : PeriodicWorkScheduler {

    /**
     * Start a recurring task, `workerClass` with the `constraints`.
     */
    override fun startWork(
        workerClass: Class<out ListenableWorker>,
        constraints: Constraints,
        intervalInMinutes: Long
    ) {
        val workRequest = PeriodicWorkRequest.Builder(workerClass, intervalInMinutes, MINUTES)
            .setConstraints(constraints)
            .build()

        val workManager = WorkManager.getInstance()

        workManager.enqueueUniquePeriodicWork(
            workerClass.name,
            ExistingPeriodicWorkPolicy.REPLACE,
            workRequest
        )
    }
}