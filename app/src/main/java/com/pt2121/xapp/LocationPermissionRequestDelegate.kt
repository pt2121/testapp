package com.pt2121.xapp

import android.Manifest
import android.Manifest.permission
import android.R.id
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import com.google.android.material.snackbar.Snackbar
import com.pt2121.xapp.R.string
import javax.inject.Inject

/**
 * This class is responsible for requesting the location permission.
 */
class LocationPermissionRequestDelegate @Inject constructor(
    private val activity: AppCompatActivity
) : LifecycleObserver {

    init {
        activity.lifecycle.addObserver(this)
    }

    private var listener: LocationPermissionRequestDelegate.Listener? = null

    /**
     * set `LocationPermissionRequestDelegate.Listener` to get a callback when the permission is granted.
     */
    fun setListener(listener: LocationPermissionRequestDelegate.Listener?) {
        this.listener = listener
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun requestPermissionIfNeeded() {
        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION)
            != PackageManager.PERMISSION_GRANTED
        ) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    activity,
                    Manifest.permission.ACCESS_FINE_LOCATION
                )
            ) {
                showRequestPermissionRationale()
            } else {
                requestLocationPermission()
            }
        } else {
            listener?.onPermissionGranted()
        }
    }

    fun onRequestPermissionsResult(
        requestCode: Int,
        grantResults: IntArray
    ) {
        if (requestCode == PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION) {
            when {
                grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED -> listener?.onPermissionGranted()
                ActivityCompat.shouldShowRequestPermissionRationale(
                    activity,
                    Manifest.permission.ACCESS_FINE_LOCATION
                )
                -> showRequestPermissionRationale()
            }
        }
    }

    /**
     * Show the rationale
     */
    private fun showRequestPermissionRationale() {
        Snackbar.make(
            activity.findViewById(id.content),
            activity.getString(string.location_permission_rationale),
            Snackbar.LENGTH_INDEFINITE
        ).setAction(activity.getString(string.location_permission_ok)) {
            requestLocationPermission()
        }.show()
    }

    private fun requestLocationPermission() {
        ActivityCompat.requestPermissions(
            activity,
            arrayOf(permission.ACCESS_FINE_LOCATION),
            PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION
        )
    }

    interface Listener {
        /**
         * A callback called when the `ACCESS_FINE_LOCATION` is granted.
         */
        fun onPermissionGranted()
    }

    companion object {
        private const val PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 3427
    }
}