package com.pt2121.xapp.utils

import android.text.SpannableString
import android.text.Spanned
import android.text.style.ClickableSpan
import android.view.View

/**
 * Attach a [ClickableSpan] to the [clickableText] of this [SpannableString]
 * with [clickListener] which will be called when the text is clicked or selected.
 */
fun SpannableString.addClickableSpan(clickableText: String, clickListener: (View) -> Unit) {
    val startIndex = indexOf(clickableText)
    val endIndex = startIndex + clickableText.length

    setSpan(object : ClickableSpan() {
        override fun onClick(widget: View) {
            clickListener(widget)
        }
    }, startIndex, endIndex, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
}