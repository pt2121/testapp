package com.pt2121.xapp.utils

import android.content.Context
import android.location.Location
import android.location.LocationManager
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import com.google.android.gms.location.LocationServices
import com.google.android.gms.tasks.Tasks

object LocationUtils {

    /**
     * Task #3
     * Try the fused location provider first then try Android API.
     * Google claimed the fused location provider is accurate and low power
     */
    fun getBestLastLocation(context: Context): Location? =
        getFusedLocation(context) ?: getLastLocation(context)

    private fun getFusedLocation(context: Context): Location? {
        val googleApiAvailability = GoogleApiAvailability.getInstance()
        val code = googleApiAvailability.isGooglePlayServicesAvailable(context)
        return if (code == ConnectionResult.SUCCESS) {
            val client = LocationServices.getFusedLocationProviderClient(context)
            try {
                Tasks.await(client.lastLocation)
            } catch (exception: SecurityException) {
                null
            } catch (interruption: InterruptedException) {
                null
            }
        } else null
    }

    private fun getLastLocation(context: Context): Location? {
        val locationManager = context.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return try {
            when {
                locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) ->
                    locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER)
                locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER) ->
                    locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER)
                else -> null
            }
        } catch (exception: SecurityException) {
            null
        } catch (e: Exception) {
            null
        }
    }
}